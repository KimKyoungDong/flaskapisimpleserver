#!/usr/bin/python
# -*- coding: utf-8 -*-
from . import db
from datetime import datetime, date
import random

def init_db():
    print "init_db"
    db.create_all()
    # testPersonel()
    Sighting()

def clear_db():
    db.drop_all()

class Sighting(db.Model):
  __tablename__ = 'sightings'
  id = db.Column(db.Integer, primary_key = True)
  sighted_at = db.Column(db.Integer)
  reported_at = db.Column(db.Integer)
  location = db.Column(db.String(100))
  shape = db.Column(db.String(10))
  duration = db.Column(db.String(10))
  description = db.Column(db.Text)
  lat = db.Column(db.Float(6))
  lng = db.Column(db.Float(6))

def testPersonel():

    #참고 : DB존재 안함
    attr = PersonnelStandardInformationAttribute('in-office-type', u'재직구분')
    data = PersonnelStandardInformationData(u'재직', 1)
    attr.data.append(data)
    data = PersonnelStandardInformationData(u'퇴사', 2)
    attr.data.append(data)
    db.session.add(attr)

    db.session.commit()

