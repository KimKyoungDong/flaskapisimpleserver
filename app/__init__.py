#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from flask import Flask, jsonify, render_template, got_request_exception
from flask.ext.sqlalchemy import SQLAlchemy
from flask import request
import json

from flask.ext.restful import Resource, Api

app = Flask(__name__)
# mysql = MySQL()
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///task.db'
# app.config['SQLALCHEMY_DATABASE_USER'] = 'root'
# app.config['SQLALCHEMY_DATABASE_PASSWORD'] = 'root'
# app.config['SQLALCHEMY_DATABASE_DB'] = 'apiserver'

# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://root:root@52.68.5.220/apiserver'
db = SQLAlchemy(app, use_native_unicode=True)
db.init_app(app)

# mysql.init_app(app)
# db = SQLAlchemy(app, use_native_unicode=True)
# db.init_app(app)

@app.route("/page/test", methods=['GET'])
def page_test():
    return render_template('layout_blank_page.html')

@app.route("/", methods=['GET'])
def home():
    result = []
    for i in range(10):
        if(i<8):
            dic = {'bool': True}
            result.append(dic)
        else:
            dic = {'bool': False}
            result.append(dic)
    # result.append(('bool', False))

    return json.dumps(result)
    # return 'hello'

@app.route("/upload", methods=['POST'])
def upload():
    bool = request.form['bool']
    return bool

@app.route("/loaddb", methods=['GET'])
def loadData():
    cursor = db.cursor()

    result = []
    columns = tuple([d[0] for d in cursor.description])

    for row in cursor:
        result.append(dict(zip(columns, row)))

    print(result)

    return json.dumps(result)



# 운전직 정보
@app.route("/page/driver/personnel", methods=['GET'])
def page_personnel():
    return render_template('personnel.html')



if __name__ == '__main__':
    app.run()
