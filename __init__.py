from flask import Flask, jsonify, render_template, request
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager
from flask.ext.migrate import Migrate
from flask.ext.restful import Api

app = Flask(__name__, static_url_path='/assets', static_folder='assets')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///task.db'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://root:!epdlxjtnwlq00@14.63.163.37/ULAS'
db = SQLAlchemy(app, use_native_unicode=True)
db.init_app(app)
migrate = Migrate(app, db)
manager = Manager(app)

class Sighting(db.Model):
  __tablename__ = 'sightings'
  id = db.Column(db.Integer, primary_key = True)
  sighted_at = db.Column(db.Integer)
  reported_at = db.Column(db.Integer)
  location = db.Column(db.String(100))
  shape = db.Column(db.String(10))
  duration = db.Column(db.String(10))
  description = db.Column(db.Text)
  lat = db.Column(db.Float(6))
  lng = db.Column(db.Float(6))

db.create_all()

@app.route("/page/test", methods=['GET'])
def page_test():
    return render_template('layout_blank_page.html')

@app.route("/", methods=['GET'])
def home():
    return 'Hello World'

if __name__ == '__main__':
    app.run()
