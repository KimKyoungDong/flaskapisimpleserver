#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jaehwacho'
from flask.ext.script import Manager
from app import app
from app.models import init_db, clear_db
from flask.ext.migrate import MigrateCommand
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

manager = Manager(app)
manager.add_command('db', MigrateCommand)

#실행방법
#python manage.py initdb
@manager.command
def initdb():
    """Initialize database."""
    with app.app_context():
        init_db()

#실행방법
#python manage.py cleardb
@manager.command
def cleardb():
    """Clear database."""
    with app.app_context():
        clear_db()

if __name__ == '__main__':
    manager.run()

