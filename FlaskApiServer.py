from flask import Flask, jsonify
from flask import Flask, request
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://apidb:12341234@api.db/'
db = SQLAlchemy(app)

class Sighting(db.Model):
  __tablename__ = 'sightings'
  id = db.Column(db.Integer, primary_key = True)
  sighted_at = db.Column(db.Integer)
  reported_at = db.Column(db.Integer)
  location = db.Column(db.String(100))
  shape = db.Column(db.String(10))
  duration = db.Column(db.String(10))
  description = db.Column(db.Text)
  lat = db.Column(db.Float(6))
  lng = db.Column(db.Float(6))

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    }
]

@app.errorhandler(404)
def not_found(error=None):
    message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp


@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/todo/api/v1.0/tasks/<userid>/test', methods=['GET'])
def get_tasks(userid):
    users = {'1':'john', '2':'steve', '3':'bill'}

    if userid in users:
        return jsonify({userid:users[userid]})
    else:
        # return jsonify({'tasks': tasks})
        return jsonify({'tasks': tasks})

@app.route('/todo/api/v1.0/rasberry', methods=['PUT'])
def put_tasks():
    return jsonify({'tasks': tasks})

@app.route('/todo/api/v1.0/sightings', methods=['GET'])
def sightings():
  if request.method == 'GET':
    results = Sighting.query.limit(10).offset(0).all()

    json_results = []
    for result in results:
      d = {'sighted_at': result.sighted_at,
           'reported_at': result.reported_at,
           'location': result.location,
           'shape': result.shape,
           'duration': result.duration,
           'description': result.description,
           'lat': result.lat,
           'lng': result.lng}
      json_results.append(d)

    return jsonify(items=json_results)

if __name__ == '__main__':
    app.run()
